# This function work like print()
# Use when you Lazy to type print() :D
def p(text):
    print(text)

p("Hello World")
p("Choose Choice")
p("1. to exit program.")